FROM python:3.8-slim

# Set the working directory to /final_proj
WORKDIR /app

# # Copy the current directory contents into the container at /final_proj
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 2456 available to the world outside this container
EXPOSE 2456

# Run database.py when the container launches
CMD ["app.py"]

ENTRYPOINT ["python3"]
